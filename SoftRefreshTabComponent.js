import { LightningElement,api } from 'lwc';
import {
    registerListener,sharedData
} from 'c/pubsub';

export default class SoftRefreshTabComponent extends LightningElement {
    @api recordId;
    connectedCallback() {
        registerListener("softRefreshTab", this.handleRefreshTab, this);
    }
    
    handleRefreshTab(obj){ 
        console.log('inside soft refresh event lwc: ');
        const filterChangeEvent = new CustomEvent('refreshtab');
        this.dispatchEvent(filterChangeEvent);
    }
}